# AngularStrap.Ui-Router.Example
Пример AngularStrap + UI Router

Здесь довольно наглядно продемонстрировано использование роутинга в <a href="https://github.com/angular-ui/ui-router" target="_blank">ui-router</a>.
<br>
Сделано с использованием <a href="https://github.com/mgcrea/angular-strap" target="_blank">angular-strap</a>.
<br>
Поcмотреть можно <a href="https://sergofan-git.gitlab.io/AngularStrap.Ui-Router" target="_blank">здесь</a>.
